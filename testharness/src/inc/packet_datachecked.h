/*
 * common.h
 *
 *  Created on: May 27, 2022
 *      Author: seb
 */

#ifndef INC_PACKET_DATACHECKED_H_
#define INC_PACKET_DATACHECKED_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "packet_define.h"

//void packet_datachecked_init_(packet_datachecked_t *self, packet_message_t *message);


#ifdef __cplusplus
}
#endif

#endif /* INC_PACKET_DATACHECKED_H_ */
