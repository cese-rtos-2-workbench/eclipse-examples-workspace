/*
 * common.h
 *
 *  Created on: May 27, 2022
 *      Author: seb
 */

#ifndef INC_PACKET_MESSAGE_H_
#define INC_PACKET_MESSAGE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "packet.h"
#include "packet_define.h"

void packet_message_process(packet_message_t *message);

//void packet_message_write(packet_message_t *message, packet_byte_t *buffer, size_t size);

#ifdef __cplusplus
}
#endif

#endif /* INC_PACKET_MESSAGE_H_ */
