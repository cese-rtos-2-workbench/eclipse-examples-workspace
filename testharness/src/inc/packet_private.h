/*
 * common.h
 *
 *  Created on: May 27, 2022
 *      Author: seb
 */

#ifndef INC_PACKET_PRIVATE_H_
#define INC_PACKET_PRIVATE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "packet_define.h"
#include "packet_protocol.h"
#include "packet_message.h"
#include "packet_datachecked.h"
#include "packet_camelcase.h"

#define ACTION_CAMELCASE_               ('C')
#define ACTION_PASCALCASE_              ('P')
#define ACTION_SNAKECASE_               ('S')

typedef enum
{
  MESSAGE_ERRORCODE_INVALID_DATA = 0,
  MESSAGE_ERRORCODE_INVALID_OPCODE = 1,
  MESSAGE_ERRORCODE_SYSTEM = 2,
} message_errorcode_t;

typedef bool (*message_action_t)(packet_message_t *message);

bool packet_action_camelcase(packet_message_t *message);

//void packet_message_init_(packet_message_t *self);
packet_message_t *packet_message_init_(packet_protocol_t *protocol);

packet_datachecked_t *packet_datachecked_init_(packet_message_t *message);

bool packet_datachecked_check_(packet_datachecked_t *self);

bool packet_camelcase_process_(packet_camelcase_t *message);

packet_camelcase_t *packet_camelcase_init_(packet_message_t *message);

#ifdef __cplusplus
}
#endif

#endif /* INC_PACKET_PRIVATE_H_ */
