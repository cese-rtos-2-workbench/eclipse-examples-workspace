/*
 * common.h
 *
 *  Created on: May 27, 2022
 *      Author: seb
 */

#ifndef INC_PACKET_DEFINE_H_
#define INC_PACKET_DEFINE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"

typedef char packet_byte_t;

typedef struct packet_datachecked_s packet_datachecked_t;

typedef struct packet_camelcase_s packet_camelcase_t;

typedef struct packet_message_s packet_message_t;

typedef struct packet_protocol_s packet_protocol_t;

struct packet_datachecked_s
{
    packet_byte_t *buffer;
    size_t size;

    uint32_t n_words;
    uint32_t n_chars;

    packet_message_t *message;
};

typedef bool (*packet_camelcase_state_t)(packet_camelcase_t *self, packet_byte_t *byte_out, packet_byte_t byte_in);

struct packet_camelcase_s
{
    packet_byte_t *buffer;
    size_t size;

    int idx_read;
    int idx_write;
    packet_camelcase_state_t state;

    packet_message_t *message;
};

struct packet_message_s
{
    packet_byte_t *buffer;
    size_t size;

    packet_datachecked_t datachecked;
    packet_camelcase_t camelcase;

    packet_protocol_t *protocol;
};

typedef enum
{
  PACKET_PROTOCOL_EVENT_NOT_EVENT,
  PACKET_PROTOCOL_EVENT_SOM_DETECTED,
  PACKET_PROTOCOL_EVENT_EOM_DETECTED,
  PACKET_PROTOCOL_EVENT_INVALID_MESSAGE,
  PACKET_PROTOCOL_EVENT_VALID_MESSAGE,
} packet_protocol_event_t;

typedef void (*packet_protocol_user_cb_t)(packet_protocol_event_t event);

typedef void (*packet_protocol_feed_handler_t)(packet_protocol_t *self, packet_byte_t byte);

struct packet_protocol_s
{
    packet_protocol_user_cb_t user_cb;
    packet_protocol_feed_handler_t feed_h;
    bool flag_valid_message;

    packet_byte_t *buffer;
    size_t max_size;
    size_t size;

    packet_message_t message;
};




//typedef struct
//{
//    packet_byte_t *packet_buffer;
//
//    packet_byte_t *buffer;
//    size_t size;
//
//    packet_datachecked_t datachecked;
//} packet_message_t;


#ifdef __cplusplus
}
#endif

#endif /* INC_PACKET_H_ */
