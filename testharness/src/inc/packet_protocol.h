/*
 * common.h
 *
 *  Created on: May 27, 2022
 *      Author: seb
 */

#ifndef INC_PACKET_PROTOCOL_H_
#define INC_PACKET_PROTOCOL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "packet_define.h"
#include "packet_message.h"
#include "packet_datachecked.h"

#define PACKET_PROTOCOL_MAX_SIZE                        (200)
#define PACKET_PROTOCOL_MIN_SIZE                        (1 + 4 + 2 + 1) // SOM + ID + A + CRC + EOM
#define PACKET_PROTOCOL_SOM                             ('(')
#define PACKET_PROTOCOL_EOM                             (')')
#define PACKET_PROTOCOL_CRC_SEED                        (0x00)

void packet_protocol_init(packet_protocol_t *self, packet_protocol_user_cb_t user_cb);

void packet_protocol_set_buffer(packet_protocol_t *self, packet_byte_t *buffer, size_t max_size);

void packet_protocol_feed(packet_protocol_t *self, packet_byte_t byte);

packet_message_t *packet_protocol_message_read(packet_protocol_t *self);

packet_protocol_t *packet_protocol_message_write(packet_message_t *message);

void packet_protocol_create(packet_protocol_t *self, uint16_t id, packet_byte_t *payload_buffer, size_t payload_size);

#ifdef __cplusplus
}
#endif

#endif /* INC_PACKET_PROTOCOL_H_ */
