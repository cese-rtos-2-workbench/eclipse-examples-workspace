/*
 * TEST Condition Checks
 * Here is a list of the TEST condition checks available in CppUTest at the
 * time of this writing. Unity and CppUTest differ in their condition check
 * names. Conceptually they are the same.
 * • CHECK(boolean condition) : Checks any boolean condition
 * • CHECK_TRUE(boolean condition) : Same as CHECK ( )
 * • CHECK_FALSE(boolean condition) : Passes for a FALSE boolean condition.
 * • CHECK(boolean condition) : Checks any boolean condition
 * • CHECK_EQUAL(expected, actual) : Checks for equality between entities using ==
 * • STRCMP_EQUAL(expected, actual) : Compares const char* strings for equality using strcmp()
 * • LONGS_EQUAL(expected, actual) : Compares two numbers
 * • BYTES_EQUAL(expected, actual) : Compares two numbers, eight bits wide
 * • POINTERS_EQUAL(expected, actual) : Compares two pointers
 * • DOUBLES_EQUAL(expected, actual, tolerance) : Compares two doubles within some tolerance
 * • FAIL(text) : Fails test and prints message
 * The checks are also known as asserts or assertions. I’ll be using all
 * three terms interchangeably. It’s important to note that the first failed
 * assertion terminates the calling test.
 */

#include "CppUTest/TestHarness.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "packet.h"

#define EVENTS_SIZE_			(100)
static packet_byte_t check_packet_protocol_[PACKET_PROTOCOL_MAX_SIZE];
static size_t check_packet_protocol_size_;
static packet_protocol_event_t check_packet_protocol_event_[EVENTS_SIZE_];
static int event_size_;

static void user_cb_(packet_protocol_event_t event)
{
  check_packet_protocol_event_[event_size_] = event;
  event_size_++;
}

#ifdef __cplusplus
}
#endif

TEST_GROUP(PacketProtocol)
{
  packet_protocol_t packet;
    packet_byte_t packet_protocol_buffer[PACKET_PROTOCOL_MAX_SIZE];

    void setup()
    {
      memset((void*)check_packet_protocol_, 0, PACKET_PROTOCOL_MAX_SIZE);
      check_packet_protocol_size_ = 0;
      event_size_ = 0;
      for (size_t i = 0; i < EVENTS_SIZE_; ++i)
      {
        check_packet_protocol_event_[i] = PACKET_PROTOCOL_EVENT_NOT_EVENT;
      }

      packet_protocol_init(&packet, user_cb_);
      packet_protocol_set_buffer(&packet, packet_protocol_buffer, PACKET_PROTOCOL_MAX_SIZE);
    }

    void teardown()
    {
    }
};

// SOM detected
TEST(PacketProtocol, Test00)
{
  packet_byte_t uart_stream[] = {'(', '0', '1',};

  for (size_t i = 0; i < sizeof(uart_stream); ++i)
  {
    packet_byte_t byte = uart_stream[i];
    packet_protocol_feed(&packet, byte);
  }

  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_SOM_DETECTED, check_packet_protocol_event_[0]);
  LONGS_EQUAL(1, event_size_);
}

// EOM detected
TEST(PacketProtocol, Test01)
{
  packet_byte_t uart_stream[] = {'(', '0', '1', ')',};

  for (size_t i = 0; i < sizeof(uart_stream); ++i)
  {
    packet_byte_t byte = uart_stream[i];
    packet_protocol_feed(&packet, byte);
  }

  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_SOM_DETECTED, check_packet_protocol_event_[0]);
  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_EOM_DETECTED, check_packet_protocol_event_[1]);
}

// not EOM without SOM
TEST(PacketProtocol, Test02)
{
  packet_byte_t uart_stream[] = {'0', '1', ')',};

  for (size_t i = 0; i < sizeof(uart_stream); ++i)
  {
    packet_byte_t byte = uart_stream[i];
    packet_protocol_feed(&packet, byte);
  }

  LONGS_EQUAL(0, event_size_);
}

// double SOM
TEST(PacketProtocol, Test03)
{
  packet_byte_t uart_stream[] = {'(', '0', '(', '0', '1',};

  for (size_t i = 0; i < sizeof(uart_stream); ++i)
  {
    packet_byte_t byte = uart_stream[i];
    packet_protocol_feed(&packet, byte);
  }

  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_SOM_DETECTED, check_packet_protocol_event_[0]);
  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_INVALID_MESSAGE, check_packet_protocol_event_[1]);
  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_SOM_DETECTED, check_packet_protocol_event_[2]);
  LONGS_EQUAL(3, event_size_);
}

// invalid menssage, min size
TEST(PacketProtocol, Test04)
{
  packet_byte_t uart_stream[] = {'(', '0', '0', '0', '0', 'C', '2', ')'};

  for (size_t i = 0; i < sizeof(uart_stream); ++i)
  {
    packet_byte_t byte = uart_stream[i];
    packet_protocol_feed(&packet, byte);
  }

  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_SOM_DETECTED, check_packet_protocol_event_[0]);
  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_EOM_DETECTED, check_packet_protocol_event_[1]);
  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_INVALID_MESSAGE, check_packet_protocol_event_[2]);
  LONGS_EQUAL(3, event_size_);
}

// invalid menssage, ID invalid, lower case
TEST(PacketProtocol, Test05)
{
  packet_byte_t uart_stream[] = {'(', '0', '0', '0', 'a', 'C', '3', '0', ')'};

  for (size_t i = 0; i < sizeof(uart_stream); ++i)
  {
    packet_byte_t byte = uart_stream[i];
    packet_protocol_feed(&packet, byte);
  }

  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_SOM_DETECTED, check_packet_protocol_event_[0]);
  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_EOM_DETECTED, check_packet_protocol_event_[1]);
  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_INVALID_MESSAGE, check_packet_protocol_event_[2]);
  LONGS_EQUAL(3, event_size_);
}

// deprecated
//// invalid menssage, action invalid != C, S or P
//TEST(PacketProtocol, Test06)
//{
//	packet_byte_t uart_stream[] = {'(', '0', '0', '0', 'a', 'c', '3', '0', ')'};
//
//	for(size_t i = 0; i < sizeof(uart_stream); ++i)
//	{
//		packet_byte_t byte = uart_stream[i];
//		packet_protocol_feed(&packet, byte);
//	}
//
//	LONGS_EQUAL(PACKET_PROTOCOL_EVENT_SOM_DETECTED, check_packet_protocol_event_[0]);
//	LONGS_EQUAL(PACKET_PROTOCOL_EVENT_EOM_DETECTED, check_packet_protocol_event_[1]);
//	LONGS_EQUAL(PACKET_PROTOCOL_EVENT_INVALID_MESSAGE, check_packet_protocol_event_[2]);
//	LONGS_EQUAL(3, event_size_);
//}

// invalid menssage, CRC invalid, lower case
TEST(PacketProtocol, Test07)
{
  packet_byte_t uart_stream[] = {'(', '0', '0', '0', 'A', 'C', '9', 'e', ')'};

  for (size_t i = 0; i < sizeof(uart_stream); ++i)
  {
    packet_byte_t byte = uart_stream[i];
    packet_protocol_feed(&packet, byte);
  }

  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_SOM_DETECTED, check_packet_protocol_event_[0]);
  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_EOM_DETECTED, check_packet_protocol_event_[1]);
  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_INVALID_MESSAGE, check_packet_protocol_event_[2]);
  LONGS_EQUAL(3, event_size_);
}

// valid empty menssage
TEST(PacketProtocol, Test08)
{
  packet_byte_t uart_stream[] = {'(', '0', '0', '0', 'A', 'C', '9', 'E', ')'};

  for (size_t i = 0; i < sizeof(uart_stream); ++i)
  {
    packet_byte_t byte = uart_stream[i];
    packet_protocol_feed(&packet, byte);
  }

  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_SOM_DETECTED, check_packet_protocol_event_[0]);
  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_EOM_DETECTED, check_packet_protocol_event_[1]);
  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_VALID_MESSAGE, check_packet_protocol_event_[2]);
  LONGS_EQUAL(3, event_size_);
}

// not empty message
TEST(PacketProtocol, Test09)
{
  packet_byte_t uart_stream[] = {'(', '0', '0', '0', 'A', 'C', 'h', 'o', 'l', 'a', ' ', 'm', 'u', 'n', 'd', 'o', 'B', '2', ')'};

  for (size_t i = 0; i < sizeof(uart_stream); ++i)
  {
    packet_byte_t byte = uart_stream[i];
    packet_protocol_feed(&packet, byte);
  }

  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_SOM_DETECTED, check_packet_protocol_event_[0]);
  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_EOM_DETECTED, check_packet_protocol_event_[1]);
  LONGS_EQUAL(PACKET_PROTOCOL_EVENT_VALID_MESSAGE, check_packet_protocol_event_[2]);
  LONGS_EQUAL(3, event_size_);
}

