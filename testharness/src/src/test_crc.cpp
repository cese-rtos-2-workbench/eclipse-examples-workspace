/*
 * TEST Condition Checks
 * Here is a list of the TEST condition checks available in CppUTest at the
 * time of this writing. Unity and CppUTest differ in their condition check
 * names. Conceptually they are the same.
 * • CHECK(boolean condition) : Checks any boolean condition
 * • CHECK_TRUE(boolean condition) : Same as CHECK ( )
 * • CHECK_FALSE(boolean condition) : Passes for a FALSE boolean condition.
 * • CHECK(boolean condition) : Checks any boolean condition
 * • CHECK_EQUAL(expected, actual) : Checks for equality between entities using ==
 * • STRCMP_EQUAL(expected, actual) : Compares const char* strings for equality using strcmp()
 * • LONGS_EQUAL(expected, actual) : Compares two numbers
 * • BYTES_EQUAL(expected, actual) : Compares two numbers, eight bits wide
 * • POINTERS_EQUAL(expected, actual) : Compares two pointers
 * • DOUBLES_EQUAL(expected, actual, tolerance) : Compares two doubles within some tolerance
 * • FAIL(text) : Fails test and prints message
 * The checks are also known as asserts or assertions. I’ll be using all
 * three terms interchangeably. It’s important to note that the first failed
 * assertion terminates the calling test.
 */

#include "CppUTest/TestHarness.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include "stdint.h"
#include "string.h"
#include "crc8.h"

#ifdef __cplusplus
}
#endif

TEST_GROUP(CRC)
{
	void setup()
	{
		crc8_init();
	}

	void teardown()
	{
	}
};

// empty message
TEST(CRC, Test00)
{
	uint8_t crc_seed = 0x00;
	uint8_t buffer_c[] = {'0', '0', '0', 'a', 'C'};
	uint8_t buffer_p[] = {'0', '0', '0', 'a', 'P'};
	uint8_t buffer_s[] = {'0', '0', '0', 'a', 'S'};
	LONGS_EQUAL(0x30, crc8_calc(crc_seed, buffer_c, sizeof(buffer_c)));
	LONGS_EQUAL(0x49, crc8_calc(crc_seed, buffer_p, sizeof(buffer_p)));
	LONGS_EQUAL(0x40, crc8_calc(crc_seed, buffer_s, sizeof(buffer_s)));
}

TEST(CRC, Test01)
{
	uint8_t crc_seed = 0x00;
	uint8_t buffer_c[] = {'0', '0', '0', 'A', 'C'};
	uint8_t buffer_p[] = {'0', '0', '0', 'A', 'P'};
	uint8_t buffer_s[] = {'0', '0', '0', 'A', 'S'};
	LONGS_EQUAL(0x9E, crc8_calc(crc_seed, buffer_c, sizeof(buffer_c)));
	LONGS_EQUAL(0xE7, crc8_calc(crc_seed, buffer_p, sizeof(buffer_p)));
	LONGS_EQUAL(0xEE, crc8_calc(crc_seed, buffer_s, sizeof(buffer_s)));
}

TEST(CRC, Test02)
{
        uint8_t crc_seed = 0x00;
        uint8_t buffer_c[] = {'0', '0', '0', 'A', 'C', 'h', 'o', 'l', 'a', ' ', 'm', 'u', 'n', 'd', 'o'};
        LONGS_EQUAL(0xB2, crc8_calc(crc_seed, buffer_c, sizeof(buffer_c)));
}

TEST(CRC, Test03)
{
        uint8_t crc_seed = 0x00;
        uint8_t buffer_c[] = {'0', '0', '0', 'A', 'h', 'o', 'l', 'a', 'M', 'u', 'n', 'd', 'o'};
        LONGS_EQUAL(0x73, crc8_calc(crc_seed, buffer_c, sizeof(buffer_c)));
}

