/*
 * packet.cpp
 *
 *  Created on: May 27, 2022
 *      Author: seb
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "packet_private.h"

static bool state_when_lowercase_(packet_camelcase_t *self, packet_byte_t *byte_out, packet_byte_t byte_in);

static bool state_when_uppercas_(packet_camelcase_t *self, packet_byte_t *byte_out, packet_byte_t byte_in);

static bool state_when_uppercas_(packet_camelcase_t *self, packet_byte_t *byte_out, packet_byte_t byte_in)
{
//  if('a' <= byte_in && byte_in <= 'z')
  if('A' <= byte_in && byte_in <= 'Z')
  {
    *byte_out = byte_in;
//    *byte_out = byte_in - ('a' - 'A');
  }
  else if((' ' == byte_in) || ('_' == byte_in))
  {
    self->state = state_when_uppercas_;
    return false;
  }
  else
  {
//    *byte_out = byte_in;
    *byte_out = byte_in - ('a' - 'A');
  }
  self->state = state_when_lowercase_;
  return true;
}

static bool state_when_lowercase_(packet_camelcase_t *self, packet_byte_t *byte_out, packet_byte_t byte_in)
{
  if('A' <= byte_in && byte_in <= 'Z')
  {
//    *byte_out = byte_in + ('a' - 'A');
    *byte_out = byte_in;
  }
  else if((' ' == byte_in) || ('_' == byte_in))
  {
    self->state = state_when_uppercas_;
    return false;
  }
  else
  {
    *byte_out = byte_in;
  }
  return true;
}

static bool state_when_start_(packet_camelcase_t *self, packet_byte_t *byte_out, packet_byte_t byte_in)
{
  if('A' <= byte_in && byte_in <= 'Z')
  {
    *byte_out = byte_in + ('a' - 'A');
  }
  else
  {
    *byte_out = byte_in;
  }
  self->state = state_when_lowercase_;
  return true;
}

bool packet_camelcase_process_(packet_camelcase_t *self)
{
  self->idx_read = 1;
  self->idx_write = 0;
  self->state = state_when_start_;

  packet_byte_t byte_in, byte_out;
  for(size_t i = 0; i < self->size; ++i)
  {
    byte_in = self->buffer[self->idx_read];
    if(true == self->state(self, &byte_out, byte_in))
    {
      self->buffer[self->idx_write] = byte_out;
      self->idx_write++;
    }
    self->idx_read++;
  }

  self->message->size = self->idx_write;

  return true;
}

packet_camelcase_t *packet_camelcase_init_(packet_message_t *message)
{
  packet_camelcase_t *self = &(message->camelcase);
  self->message = message;
  self->buffer = self->message->buffer;
  self->size = self->message->size - 1;
  return self;
}

#ifdef __cplusplus
}
#endif
