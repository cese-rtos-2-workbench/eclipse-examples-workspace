/*
 * TEST Condition Checks
 * Here is a list of the TEST condition checks available in CppUTest at the
 * time of this writing. Unity and CppUTest differ in their condition check
 * names. Conceptually they are the same.
 * • CHECK(boolean condition) : Checks any boolean condition
 * • CHECK_TRUE(boolean condition) : Same as CHECK ( )
 * • CHECK_FALSE(boolean condition) : Passes for a FALSE boolean condition.
 * • CHECK(boolean condition) : Checks any boolean condition
 * • CHECK_EQUAL(expected, actual) : Checks for equality between entities using ==
 * • STRCMP_EQUAL(expected, actual) : Compares const char* strings for equality using strcmp()
 * • LONGS_EQUAL(expected, actual) : Compares two numbers
 * • BYTES_EQUAL(expected, actual) : Compares two numbers, eight bits wide
 * • POINTERS_EQUAL(expected, actual) : Compares two pointers
 * • DOUBLES_EQUAL(expected, actual, tolerance) : Compares two doubles within some tolerance
 * • FAIL(text) : Fails test and prints message
 * The checks are also known as asserts or assertions. I’ll be using all
 * three terms interchangeably. It’s important to note that the first failed
 * assertion terminates the calling test.
 */

#include "CppUTest/TestHarness.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include "stdint.h"
#include "string.h"

#ifdef __cplusplus
}
#endif

TEST_GROUP(Base)
{
	void setup()
	{
	}

	void teardown()
	{
	}
};

//TEST(Base, Test00)
//{
//	CHECK_TRUE(false);
//}

