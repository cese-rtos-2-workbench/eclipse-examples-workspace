/*
 * packet.cpp
 *
 *  Created on: May 27, 2022
 *      Author: seb
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "crc8.h"
#include "packet_private.h"

static inline bool is_som_(packet_byte_t byte);

static inline bool is_eom_(packet_byte_t byte);

static bool is_hexa_char_(packet_byte_t byte);

static uint8_t hexa_val_(packet_byte_t byte);

static void feed_when_waintig_for_som_(packet_protocol_t *self, packet_byte_t byte);

static void feed_when_waintig_for_eom_(packet_protocol_t *self, packet_byte_t byte);

static void add_byte_(packet_protocol_t *self, packet_byte_t byte);

static bool check_message_(packet_protocol_t *self);

// ----------------------------------------------------------------------------

static inline bool is_som_(packet_byte_t byte)
{
  return (PACKET_PROTOCOL_SOM == byte);
}

static inline bool is_eom_(packet_byte_t byte)
{
  return (PACKET_PROTOCOL_EOM == byte);
}

static bool is_hexa_char_(packet_byte_t byte)
{
  if(('0' <= byte && byte <= '9') || ('A' <= byte && byte <= 'F'))
  {
    return true;
  }
  return false;
}

static uint8_t hexa_to_int_(packet_byte_t byte)
{
  if('0' <= byte && byte <= '9')
  {
    return (uint8_t)(byte - '0');
  }
  else if('A' <= byte && byte <= 'F')
  {
    return (uint8_t)(byte - 'A') + 10;
  }
  return 0;
}

static packet_byte_t int_to_hexa_(uint8_t value)
{
  if(0 <= value && value <= 9)
  {
    return '0' + value;
  }
  else if(10 <= value && value <= 15)
  {
    return 'A' + value - 10;
  }
  return 'x';
}

static void feed_when_waintig_for_som_(packet_protocol_t *self, packet_byte_t byte)
{
  if (true == is_som_(byte))
  {
    self->user_cb(PACKET_PROTOCOL_EVENT_SOM_DETECTED);

    self->size = 0;
    self->buffer[self->size] = byte;
    self->size++;

    self->feed_h = feed_when_waintig_for_eom_;
  }
}

static void feed_when_waintig_for_eom_(packet_protocol_t *self, packet_byte_t byte)
{
  if (true == is_som_(byte))
  {
    self->user_cb(PACKET_PROTOCOL_EVENT_INVALID_MESSAGE);

    self->user_cb(PACKET_PROTOCOL_EVENT_SOM_DETECTED);

    self->size = 0;

    add_byte_(self, byte);
  }
  else if (true == is_eom_(byte))
  {
    self->user_cb(PACKET_PROTOCOL_EVENT_EOM_DETECTED);

    add_byte_(self, byte);

    if(true == check_message_(self))
    {
      self->user_cb(PACKET_PROTOCOL_EVENT_VALID_MESSAGE);
      self->flag_valid_message = true;
    }
    else
    {
      self->user_cb(PACKET_PROTOCOL_EVENT_INVALID_MESSAGE);
    }
  }
  else
  {
    add_byte_(self, byte);
  }
}

static void add_byte_(packet_protocol_t *self, packet_byte_t byte)
{
  if(self->size < PACKET_PROTOCOL_MAX_SIZE)
  {
    self->buffer[self->size] = byte;
    self->size++;
  }
  else
  {
    self->user_cb(PACKET_PROTOCOL_EVENT_INVALID_MESSAGE);

    self->size = 0;

    self->feed_h = feed_when_waintig_for_som_;
  }
}

static bool check_message_(packet_protocol_t *self)
{
  // min size
  {
    if(self->size < PACKET_PROTOCOL_MIN_SIZE)
    {
      return false;
    }
  }

  // som
  {
    uint32_t i_som = 0;
    if(PACKET_PROTOCOL_SOM != self->buffer[i_som])
    {
      return false;
    }
  }

  // eom
  {
    uint32_t i_eom = self->size - 1;
    if(PACKET_PROTOCOL_EOM != self->buffer[i_eom])
    {
      return false;
    }
  }

  // ID
  {
    uint32_t is = 1;
    uint32_t ie = is + 4;
    for(uint32_t i = is; i < ie; ++i)
    {
      if(false == is_hexa_char_(self->buffer[i]))
      {
        return false;
      }
    }
  }

  // depecated
//  // action
//  {
//    uint32_t i_action = 5;
//    if((PACKET_ACTION_CAMELCASE != self->buffer[i_action])
//        && (PACKET_ACTION_PASCALCASE != self->buffer[i_action])
//        && (PACKET_ACTION_SNAKECASE != self->buffer[i_action]))
//    {
//      return false;
//    }
//  }

  // CRC
  {
    uint32_t is = self->size - 3;
    uint32_t ie = is + 2;
    for(uint32_t i = is; i < ie; ++i)
    {
      if(false == is_hexa_char_(self->buffer[i]))
      {
        return false;
      }
    }
  }

  // CRC value
  {
    uint32_t i_crc = self->size - 3;
    uint8_t message_crc = hexa_to_int_(self->buffer[i_crc]) * 16 + hexa_to_int_(self->buffer[i_crc + 1]);

    uint32_t i_id = 1;
    size_t message_size = self->size - (1 + 2 + 1); // SOM CRC EOF
    uint8_t calculated_crc = crc8_calc(PACKET_PROTOCOL_CRC_SEED, self->buffer + i_id, message_size);

    if(calculated_crc != message_crc)
    {
      return false;
    }
  }

  return true;
}

// ----------------------------------------------------------------------------

//void packet_init(packet_t *self, packet_user_cb_t user_cb)
void packet_protocol_init(packet_protocol_t *self, packet_protocol_user_cb_t user_cb)
{
  crc8_init();
  self->user_cb = user_cb;
  self->flag_valid_message = false;
  packet_protocol_set_buffer(self, NULL, 0);
}

void packet_protocol_set_buffer(packet_protocol_t *self, packet_byte_t *buffer, size_t max_size)
{
  self->buffer = buffer;
  self->max_size = max_size;
  self->size = 0;
  self->feed_h = feed_when_waintig_for_som_;
}

void packet_protocol_feed(packet_protocol_t *self, packet_byte_t byte)
{
  if(true == self->flag_valid_message)
  {
    return;
  }

  self->feed_h(self, byte);
}

packet_message_t *packet_protocol_message_read(packet_protocol_t *self)
{
  if(false == self->flag_valid_message)
  {
    return NULL;
  }

  packet_message_init_(self);

  packet_message_t *message = &(self->message);
  message->protocol = self;
  message->buffer = self->buffer + 5;
  message->size = self->size - PACKET_PROTOCOL_MIN_SIZE;

  return message;
}

packet_protocol_t *packet_protocol_message_write(packet_message_t *message)
{
  packet_protocol_t *self = message->protocol;
//  packet_message_init_(message);

  self->size = 1 + 4 + message->size + 2 + 1;

  uint8_t crc = crc8_calc(PACKET_PROTOCOL_CRC_SEED, self->buffer + 1, message->size + 4);
  self->buffer[self->size - 3] = int_to_hexa_((crc >> 4) & 0x0F);
  self->buffer[self->size - 2] = int_to_hexa_((crc >> 0) & 0x0F);
  self->buffer[self->size - 1] = PACKET_PROTOCOL_EOM;

  return self;
}

void packet_protocol_create(packet_protocol_t *self, uint16_t id, packet_byte_t *payload_buffer, size_t payload_size)
{
  self->buffer[0] = PACKET_PROTOCOL_SOM;

  self->buffer[0 + 1] = int_to_hexa_((id >> 12) & 0x0F);
  self->buffer[0 + 2] = int_to_hexa_((id >> 8) & 0x0F);
  self->buffer[0 + 3] = int_to_hexa_((id >> 4) & 0x0F);
  self->buffer[0 + 4] = int_to_hexa_((id >> 0) & 0x0F);

  memcpy((void*)(self->buffer + 5), (void*)payload_buffer, payload_size);
  self->size = payload_size + PACKET_PROTOCOL_MIN_SIZE;

  packet_message_t *message = packet_protocol_message_read(self);
  packet_protocol_message_write(message);

  self->flag_valid_message = true;
}

#ifdef __cplusplus
}
#endif
