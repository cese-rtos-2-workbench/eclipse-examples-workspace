/*
 * packet.cpp
 *
 *  Created on: May 27, 2022
 *      Author: seb
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "packet_private.h"

#define MIN_WORD_LEN_                   (1)
#define MAX_WORD_LEN_                   (10)
#define MIN_WORDS_                      (1)
#define MAX_WORDS_                      (15)

static bool check_byte_(packet_datachecked_t *self, int idx)
{
  packet_byte_t byte = self->buffer[idx];

  // check char
  {
    if(!(('a' <= byte && byte <= 'z')
        || ('A' <= byte && byte <= 'Z')
        || ('_' == byte)
        || (' ' == byte)))
    {
      return false;
    }
  }

  // check double "_" and " "
  {
    if((0 < idx) && (('_' == byte) || (' ' == byte)))
    {
      packet_byte_t prev_byte = self->buffer[idx - 1];
      if(('_' == prev_byte) || (' ' == prev_byte))
      {
        return false;
      }
    }
  }

  // check start with "_" and " "
  {
    if((0 == idx) && (('_' == byte) || (' ' == byte)))
    {
      return false;
    }
  }

  // check end with "_" and " "
  {
    if(((self->message->size - 2) == idx) && (('_' == byte) || (' ' == byte)))
    {
      return false;
    }
  }

  // check word len
  {
    if(('_' == byte) || (' ' == byte))
    {
      if(self->n_chars < 1)
      {
        return false;
      }
      self->n_chars = 0;
    }
    else
    {
      self->n_chars++;
      if(MAX_WORD_LEN_ < self->n_chars)
      {
        return false;
      }
    }
  }

  // check words amount
  {
    if(0 == idx)
    {
      if(('_' != byte) && (' ' != byte))
      {
        self->n_words++;
      }
    }
    else
    {
      if(('_' != byte) && (' ' != byte))
      {
        packet_byte_t prev_byte = self->buffer[idx - 1];
        if(('_' == prev_byte) || (' ' == prev_byte))
        {
          self->n_words++;
        }
      }
    }

    if(MAX_WORDS_ < self->n_words)
    {
      return false;
    }
    if(self->n_words < 1)
    {
      return false;
    }
  }

  return true;
}


packet_datachecked_t *packet_datachecked_init_(packet_message_t *message)
{
  packet_datachecked_t *self = &(message->datachecked);
  self->message = message;
  self->buffer = self->message->buffer + 1;
  self->size = self->message->size - 1;
  return self;
}

bool packet_datachecked_check_(packet_datachecked_t *self)
{
  self->n_words = 0;
  self->n_chars = 0;

  if(0 == self->size)
  {
    return false;
  }

  for(int i = 0; i < self->size; ++i)
  {
    if(false == check_byte_(self, i))
    {
      return false;
    }
  }
  return true;
}
//
//bool packet_datachecked_process(packet_datachecked_t *self, packet_byte_t byte)
//{
//  return false;
//}
//
//static bool datachecked_process(packet_byte_t byte)
//{
//  return false;
//}
//
//bool packet_check_byte(packet_byte_t byte)
//{
//  return false;
//}

#ifdef __cplusplus
}
#endif
