/*
 * TEST Condition Checks
 * Here is a list of the TEST condition checks available in CppUTest at the
 * time of this writing. Unity and CppUTest differ in their condition check
 * names. Conceptually they are the same.
 * • CHECK(boolean condition) : Checks any boolean condition
 * • CHECK_TRUE(boolean condition) : Same as CHECK ( )
 * • CHECK_FALSE(boolean condition) : Passes for a FALSE boolean condition.
 * • CHECK(boolean condition) : Checks any boolean condition
 * • CHECK_EQUAL(expected, actual) : Checks for equality between entities using ==
 * • STRCMP_EQUAL(expected, actual) : Compares const char* strings for equality using strcmp()
 * • LONGS_EQUAL(expected, actual) : Compares two numbers
 * • BYTES_EQUAL(expected, actual) : Compares two numbers, eight bits wide
 * • POINTERS_EQUAL(expected, actual) : Compares two pointers
 * • DOUBLES_EQUAL(expected, actual, tolerance) : Compares two doubles within some tolerance
 * • FAIL(text) : Fails test and prints message
 * The checks are also known as asserts or assertions. I’ll be using all
 * three terms interchangeably. It’s important to note that the first failed
 * assertion terminates the calling test.
 */

#include "CppUTest/TestHarness.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "packet.h"
#include "packet_private.h"

#define MESSAGE_INVALID_DATA_                   {'E', '0', '0',}
#define MESSAGE_INVALID_OPCODE_                 {'E', '0', '1',}

#ifdef __cplusplus
}
#endif

TEST_GROUP(PacketMessage)
{
    packet_byte_t buffer[PACKET_PROTOCOL_MAX_SIZE];
    packet_message_t message;

    void setup()
    {
      message.buffer = buffer;
//      message.size = 0;
//      packet_message_init_(&message);
    }

    void teardown()
    {
    }

    void check_message_buffer_(packet_byte_t *buffer_out, size_t size)
    {
      LONGS_EQUAL(size, message.size);
      for (size_t i = 0; i < size; ++i)
      {
        LONGS_EQUAL(buffer_out[i], message.buffer[i]);
      }
    }
};

#define GENERATE_SYNTHETIC_MESSAGE_(buffer_in)\
  memcpy((void*)buffer, (void*)(buffer_in), sizeof(buffer_in));\
  message.size = sizeof(buffer_in);\
  packet_datachecked_init_(&message)
//  packet_message_init_(&message)

#define CHECK_MESSAGE_BUFFER_(buffer_out) check_message_buffer_((buffer_out), sizeof((buffer_out)))

// check generate_synthetic_message_
TEST(PacketMessage, Test00)
{
  packet_byte_t uart_stream_in[] = {'C', 'h', 'o', 'l', 'a', ' ', 'm', 'u', 'n', 'd', 'o', };
  GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

  POINTERS_EQUAL(buffer, message.buffer);
  LONGS_EQUAL(11, message.size);
}

// invalid action
TEST(PacketMessage, Test01)
{
  packet_byte_t uart_stream_in[] = {'c', 'h'};
  GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

  packet_message_process(&message);

  packet_byte_t uart_stream_out[] = {'E', '0', '1',};
  CHECK_MESSAGE_BUFFER_(uart_stream_out);
}

// invalid data, invalid caracter
TEST(PacketMessage, Test02)
{
  packet_byte_t uart_stream_in[] = {'C', '?',};
  GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

  packet_message_process(&message);

  packet_byte_t uart_stream_out[] = MESSAGE_INVALID_DATA_;
  CHECK_MESSAGE_BUFFER_(uart_stream_out);
}

// invalid data, double "_"
TEST(PacketMessage, Test03)
{
  packet_byte_t uart_stream_in[] = {'C', '_', '_',};
  GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

  packet_message_process(&message);

  packet_byte_t uart_stream_out[] = MESSAGE_INVALID_DATA_;
  CHECK_MESSAGE_BUFFER_(uart_stream_out);
}

// invalid data, double " "
TEST(PacketMessage, Test04)
{
  packet_byte_t uart_stream_in[] = {'C', ' ', ' ',};
  GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

  packet_message_process(&message);

  packet_byte_t uart_stream_out[] = MESSAGE_INVALID_DATA_;
  CHECK_MESSAGE_BUFFER_(uart_stream_out);
}

// invalid data, "_" at the end
TEST(PacketMessage, Test05)
{
  packet_byte_t uart_stream_in[] = {'C', 'h', '_',};
  GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

  packet_message_process(&message);

  packet_byte_t uart_stream_out[] = MESSAGE_INVALID_DATA_;
  CHECK_MESSAGE_BUFFER_(uart_stream_out);
}

// invalid data, " " at the end
TEST(PacketMessage, Test06)
{
  packet_byte_t uart_stream_in[] = {'C', 'h', ' ',};
  GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

  packet_message_process(&message);

  packet_byte_t uart_stream_out[] = MESSAGE_INVALID_DATA_;
  CHECK_MESSAGE_BUFFER_(uart_stream_out);
}

// invalid data, min word size and min word amount
TEST(PacketMessage, Test07)
{
  packet_byte_t uart_stream_in[] = {'C',};
  GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

  packet_message_process(&message);

  packet_byte_t uart_stream_out[] = MESSAGE_INVALID_DATA_;
  CHECK_MESSAGE_BUFFER_(uart_stream_out);
}

// invalid data, max word size (10 < 12)
TEST(PacketMessage, Test08)
{
  packet_byte_t uart_stream_in[] = {'C', 'h', 'o', 'l', 'a', 'h', 'o', 'l', 'a', 'h', 'o', 'l', 'a',};
  GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

  packet_message_process(&message);

  packet_byte_t uart_stream_out[] = MESSAGE_INVALID_DATA_;
  CHECK_MESSAGE_BUFFER_(uart_stream_out);
}

// invalid data, max word amount (15 < 16)
TEST(PacketMessage, Test09)
{
  // seprated by " "
  {
    packet_byte_t uart_stream_in[] = {'C', 'h', ' ', 'h', ' ', 'h', ' ', 'h', ' ', 'h', ' ', 'h', ' ', 'h', ' ', 'h',
                                      ' ', 'h', ' ', 'h', ' ', 'h', ' ', 'h', ' ', 'h', ' ', 'h', ' ', 'h', ' ', 'h',
                                      ' ', };
    GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

    packet_message_process(&message);

    packet_byte_t uart_stream_out[] = MESSAGE_INVALID_DATA_;
    CHECK_MESSAGE_BUFFER_(uart_stream_out);
  }

  // seprated by "_"
  {
    packet_byte_t uart_stream_in[] = {'C', 'h', '_', 'h', '_', 'h', '_', 'h', '_', 'h', '_', 'h', '_', 'h', '_', 'h',
                                      '_', 'h', '_', 'h', '_', 'h', '_', 'h', '_', 'h', '_', 'h', '_', 'h', '_', 'h',
                                      '_', };
    GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

    packet_message_process(&message);

    packet_byte_t uart_stream_out[] = MESSAGE_INVALID_DATA_;
    CHECK_MESSAGE_BUFFER_(uart_stream_out);
  }
}

//// get the message
//TEST(Packet, Test10)
//{
//  packet_byte_t uart_stream[] = {'(', '0', '0', '0', 'A', 'C', 'h', 'o', 'l', 'a', ' ', 'm', 'u', 'n', 'd', 'o', 'B', '2', ')'};
//
//  for (size_t i = 0; i < sizeof(uart_stream); ++i)
//  {
//    packet_byte_t byte = uart_stream[i];
//    packet_feed(&packet, byte);
//  }
//
//  packet_message_t message;
//  packet_get_message(&packet, &message);
//
//  LONGS_EQUAL(PACKET_EVENT_SOM_DETECTED, check_packet_event_[0]);
//  LONGS_EQUAL(PACKET_EVENT_EOM_DETECTED, check_packet_event_[1]);
//  LONGS_EQUAL(PACKET_EVENT_VALID_MESSAGE, check_packet_event_[2]);
//
//  POINTERS_EQUAL(packet_buffer, message.packet_buffer);
//  POINTERS_EQUAL(packet_buffer + 5, message.buffer);
//  LONGS_EQUAL(11, message.size);
//
//  LONGS_EQUAL(3, event_size_);
//}

