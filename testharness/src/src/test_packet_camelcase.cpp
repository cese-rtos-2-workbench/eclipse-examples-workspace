/*
 * TEST Condition Checks
 * Here is a list of the TEST condition checks available in CppUTest at the
 * time of this writing. Unity and CppUTest differ in their condition check
 * names. Conceptually they are the same.
 * • CHECK(boolean condition) : Checks any boolean condition
 * • CHECK_TRUE(boolean condition) : Same as CHECK ( )
 * • CHECK_FALSE(boolean condition) : Passes for a FALSE boolean condition.
 * • CHECK(boolean condition) : Checks any boolean condition
 * • CHECK_EQUAL(expected, actual) : Checks for equality between entities using ==
 * • STRCMP_EQUAL(expected, actual) : Compares const char* strings for equality using strcmp()
 * • LONGS_EQUAL(expected, actual) : Compares two numbers
 * • BYTES_EQUAL(expected, actual) : Compares two numbers, eight bits wide
 * • POINTERS_EQUAL(expected, actual) : Compares two pointers
 * • DOUBLES_EQUAL(expected, actual, tolerance) : Compares two doubles within some tolerance
 * • FAIL(text) : Fails test and prints message
 * The checks are also known as asserts or assertions. I’ll be using all
 * three terms interchangeably. It’s important to note that the first failed
 * assertion terminates the calling test.
 */

#include "CppUTest/TestHarness.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "packet.h"
#include "packet_private.h"

#ifdef __cplusplus
}
#endif

TEST_GROUP(PacketCamelCase)
{
    packet_byte_t buffer[PACKET_PROTOCOL_MAX_SIZE];
    packet_message_t message;

    void setup()
    {
      message.buffer = buffer;
    }

    void teardown()
    {
    }

    void check_message_buffer_(packet_byte_t *buffer_out, size_t size)
    {
      LONGS_EQUAL(size, message.size);
      for (size_t i = 0; i < size; ++i)
      {
        LONGS_EQUAL(buffer_out[i], message.buffer[i]);
      }
    }
};

#define GENERATE_SYNTHETIC_MESSAGE_(buffer_in)\
  memcpy((void*)buffer, (void*)(buffer_in), sizeof(buffer_in));\
  message.size = sizeof(buffer_in);\
  packet_datachecked_init_(&message)
//  packet_message_init_(&message)

#define CHECK_MESSAGE_BUFFER_(buffer_out) check_message_buffer_((buffer_out), sizeof((buffer_out)))

// valid data, 1 word
TEST(PacketCamelCase, Test01)
{
  packet_byte_t uart_stream_in[] = {'C', 'h', 'o', 'l', 'a',};
  GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

  packet_message_process(&message);

  packet_byte_t uart_stream_out[] = {'h', 'o', 'l', 'a',};
  CHECK_MESSAGE_BUFFER_(uart_stream_out);
}

// valid data, 2 word
TEST(PacketCamelCase, Test02)
{
  packet_byte_t uart_stream_in[] = {'C', 'h', 'o', 'l', 'a', ' ', 'm', 'u', 'n', 'd', 'o',};
  GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

  packet_message_process(&message);

  packet_byte_t uart_stream_out[] = {'h', 'o', 'l', 'a', 'M', 'u', 'n', 'd', 'o',};
  CHECK_MESSAGE_BUFFER_(uart_stream_out);
}

// valid data, 2 word
TEST(PacketCamelCase, Test03)
{
  packet_byte_t uart_stream_in[] = {'C', 'h', 'o', 'l', 'a', '_', 'm', 'u', 'n', 'd', 'o',};
  GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

  packet_message_process(&message);

  packet_byte_t uart_stream_out[] = {'h', 'o', 'l', 'a', 'M', 'u', 'n', 'd', 'o',};
  CHECK_MESSAGE_BUFFER_(uart_stream_out);
}

// valid data, 2 word
TEST(PacketCamelCase, Test04)
{
  packet_byte_t uart_stream_in[] = {'C', 'A', 'A', 'A', 'A', '_', 'B', 'B',};
  GENERATE_SYNTHETIC_MESSAGE_(uart_stream_in);

  packet_message_process(&message);

  packet_byte_t uart_stream_out[] = {'a', 'A', 'A', 'A', 'B', 'B',};
  CHECK_MESSAGE_BUFFER_(uart_stream_out);
}

