/*
 * TEST Condition Checks
 * Here is a list of the TEST condition checks available in CppUTest at the
 * time of this writing. Unity and CppUTest differ in their condition check
 * names. Conceptually they are the same.
 * • CHECK(boolean condition) : Checks any boolean condition
 * • CHECK_TRUE(boolean condition) : Same as CHECK ( )
 * • CHECK_FALSE(boolean condition) : Passes for a FALSE boolean condition.
 * • CHECK(boolean condition) : Checks any boolean condition
 * • CHECK_EQUAL(expected, actual) : Checks for equality between entities using ==
 * • STRCMP_EQUAL(expected, actual) : Compares const char* strings for equality using strcmp()
 * • LONGS_EQUAL(expected, actual) : Compares two numbers
 * • BYTES_EQUAL(expected, actual) : Compares two numbers, eight bits wide
 * • POINTERS_EQUAL(expected, actual) : Compares two pointers
 * • DOUBLES_EQUAL(expected, actual, tolerance) : Compares two doubles within some tolerance
 * • FAIL(text) : Fails test and prints message
 * The checks are also known as asserts or assertions. I’ll be using all
 * three terms interchangeably. It’s important to note that the first failed
 * assertion terminates the calling test.
 */

#include "CppUTest/TestHarness.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "packet.h"

//#define EVENTS_SIZE_			(100)
//static packet_byte_t check_packet_protocol_[PACKET_PROTOCOL_MAX_SIZE];
//static size_t check_packet_protocol_size_;
//static packet_protocol_event_t check_packet_protocol_event_[EVENTS_SIZE_];
//static int event_size_;

static void user_cb_(packet_protocol_event_t event)
{
//  check_packet_protocol_event_[event_size_] = event;
//  event_size_++;
}

#ifdef __cplusplus
}
#endif

TEST_GROUP(PacketSystem)
{
    packet_protocol_t protocol;
    packet_byte_t packet_protocol_buffer[PACKET_PROTOCOL_MAX_SIZE];

    void setup()
    {
//      memset((void*)check_packet_protocol_, 0, PACKET_PROTOCOL_MAX_SIZE);
//      check_packet_protocol_size_ = 0;
//      event_size_ = 0;
//      for (size_t i = 0; i < EVENTS_SIZE_; ++i)
//      {
//        check_packet_protocol_event_[i] = PACKET_PROTOCOL_EVENT_NOT_EVENT;
//      }

      packet_protocol_init(&protocol, user_cb_);
      packet_protocol_set_buffer(&protocol, packet_protocol_buffer, PACKET_PROTOCOL_MAX_SIZE);
    }

    void teardown()
    {
    }

    void check_buffer_(packet_protocol_t *protocol, packet_byte_t *buffer_out, size_t size)
    {
      LONGS_EQUAL(size, protocol->size);
      for (size_t i = 0; i < size; ++i)
      {
        LONGS_EQUAL(buffer_out[i], protocol->buffer[i]);
      }
    }
};

// final test
TEST(PacketSystem, Test00)
{
  packet_byte_t uart_stream[] = {'(', '0', '0', '0', 'A', 'C', 'h', 'o', 'l', 'a', ' ', 'm', 'u', 'n', 'd', 'o', 'B', '2', ')'};

  for (size_t i = 0; i < sizeof(uart_stream); ++i)
  {
    packet_byte_t byte = uart_stream[i];
    packet_protocol_feed(&protocol, byte);
  }

  packet_message_t *message = packet_protocol_message_read(&protocol);
  packet_message_process(message);
  packet_protocol_t *new_protocol = packet_protocol_message_write(message);

  POINTERS_EQUAL(&protocol, new_protocol);

  packet_byte_t uart_stream_out[] = {'(', '0', '0', '0', 'A', 'h', 'o', 'l', 'a', 'M', 'u', 'n', 'd', 'o', '7', '3', ')'};
  check_buffer_(new_protocol, uart_stream_out, sizeof(uart_stream_out));
}

